package main

import (
	"context"
	"net"

	greeter "example.com/plz-go-mod/greeter/proto"
	"google.golang.org/grpc"

	"github.com/sirupsen/logrus"
)

var log = logrus.New()

type server struct{}

func (s *server) Hello(ctx context.Context, request *greeter.Request) (*greeter.Response, error) {
	log.Infof("Received: %v", request.GetName())

	return &greeter.Response{
		Msg: "hello " + request.GetName(),
	}, nil
}

func main() {
	listener, err := net.Listen("tcp", ":3000")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	greeter.RegisterGreeterServer(s, &server{})

	if err := s.Serve(listener); err != nil {
		log.Fatalf("failed to server: %v", err)
	}
}
