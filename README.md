# please-go-mod-example

This is a quick and dirty example of how you could use go modules with
https://please.build

## go mod tidy

Normally go mod tidy would look through the plz-out directory to determine how
to clean up your dependency graph. We don't want this to happen as there can be
third party go files in there that we don't actually use in our code.

`make go-mod-tidy` tries to work around this issue by attempting to remove
offending files from plz-out before running `go mod tidy`.

## -mod=readonly

`go build` commands get run with `-mod=readonly`. This makes sure that the
`go.mod` file in your project implicitly declares all the projects dependencies.
