.PHONY: go-mod-tidy
go-mod-tidy:
	plz build //:go_mod
	cd plz-out && \
		rm -rf tmp \
			bin \
			gen/third_party
	go mod tidy
	plz build //:go_mod