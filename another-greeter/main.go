package main

import (
	greeter "example.com/plz-go-mod/greeter/proto"
	"github.com/davecgh/go-spew/spew"
)

func main() {
	spew.Dump(greeter.Response{
		Msg: hello("Jack"),
	})
}
