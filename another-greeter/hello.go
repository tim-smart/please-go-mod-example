package main

import "fmt"

func hello(name string) string {
	return fmt.Sprintf("Hi %s", name)
}
