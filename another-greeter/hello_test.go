package main

import "testing"

func TestHello(t *testing.T) {
	if hello("name") != "Hi name" {
		t.Error("hello(\"name\") failed")
	}
}
