module example.com/plz-go-mod

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/golang/protobuf v1.3.2
	github.com/nsf/gocode v0.0.0-20190302080247-5bee97b48836
	github.com/sirupsen/logrus v1.4.2
	google.golang.org/grpc v1.25.1
)
